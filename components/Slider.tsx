'use client'
import { useEffect, useState, useCallback } from "react";
import Slider from "react-animated-slider";
import "react-animated-slider/build/horizontal.css";
import "./ImageSlider.css";
import { EndPoint } from "@/app/constants";

interface SliderItem {
  name: string;
  discount_percentage: number;
  image: string;
}

const ImageSlider = () => {
  const [venues, setVenues] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [discountPercentage, setDiscountPercentage] = useState('');

  // Debounce input changes
  const debounce = (func: Function, delay: number) => {
    let timer: any;
    return (...args: any[]) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        func(...args);
      }, delay);
    };
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const debouncedFetchImages = useCallback(
    debounce(async (page: number, discount: number) => {
      let queryParams = `page=${page}`;
      if (discount >= 5 && discount <= 95) {
        queryParams += `&discount_percentage=${discount}`;
      }
      try {
        const response = await fetch(`${EndPoint.Venues}?${queryParams}`);
        if (response.ok) {
          const data = await response.json();
          setVenues(data?.data);
          setHasNextPage(data?.data?.length > 0);
        } else {
          console.error("Failed to fetch images");
        }
      } catch (error) {
        console.error("Error fetching images:", error);
      }
    }, 500), // Debounce delay of 500ms
    []
  );

  useEffect(() => {
    debouncedFetchImages(pageNumber, +discountPercentage);
  }, [pageNumber, discountPercentage, debouncedFetchImages]);

  const handlePrevPage = () => {
    if (pageNumber > 1) {
      setPageNumber(pageNumber - 1);
    }
  };

  const handleNextPage = () => {
    if (hasNextPage) {
      setPageNumber(pageNumber + 1);
    }
  };

  const handleDiscountChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value.trim();
      setDiscountPercentage(value);
  };
  
  return (
    <>
      <input
        className="input-discount"
        type="number"
        value={discountPercentage}
        onChange={handleDiscountChange}
        placeholder="Enter discount percentage 5% - 95%"
        max={95}
        min={5}
      />

      <Slider autoplay={3000}>
        {venues?.map((item: SliderItem, index) => (
          <div
            key={index}
            style={{
              background: `url('${item.image}') no-repeat center center`,
              backgroundSize: "cover",
              width: "100%",
              height: "400px",
            }}
          >
            <div
              style={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                textAlign: "center",
                backgroundColor: "rgba(0, 0, 0, 0.5)",
                color: "white",
                padding: "10px 20px",
                borderRadius: "5px",
                border: "none",
                width: "100%",
                textShadow: "2px 2px 4px rgba(0,0,0,0.5)",
                fontSize: "bold",
              }}
            >
              <h1>{item.name}</h1>
              <button>{item.discount_percentage}%</button>
            </div>
          </div>
        ))}
      </Slider>

      <div className="pagination-buttons">
        <button className="pagination-button" onClick={handlePrevPage}>
          Prev
        </button>
        <button
          className="pagination-button"
          onClick={handleNextPage}
          disabled={venues.length === 0}
        >
          Next
        </button>
      </div>
    </>
  );
};

export default ImageSlider;
